#include <wiringPi.h>
#include <stdio.h>

#define LED 17

int main() {
  wiringPiSetupGpio();
  pinMode(LED, OUTPUT);

  for (int i = 0; i < 50; i++) {
    digitalWrite(LED, HIGH);
    delay (500);
    digitalWrite(LED, LOW);
    delay (500);
  }


  return 0;
}
