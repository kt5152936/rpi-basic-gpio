# Raspberry Pi GPIO with C

## General

This project involved using the Raspberry Pi Model 3B+. Support for other models is not guaranteed. Software and GPIO pinouts may differ.
The WiringPi library is used for accessing the GPIO pins.

## Installation

### WiringPi

See https://github.com/WiringPi/WiringPi?tab=readme-ov-file#installing for installation guide.

sudo apt install git
git clone https://github.com/WiringPi/WiringPi.git
cd WiringPi

// build the package
./build debian
mv debian-template/wiringpi-3.0-1.deb .

// install it
sudo apt install ./wiringpi-3.0-1.deb

## Startup

### Compilation

gcc -O2 -Wall -g -o main main.c -l wiringPi
